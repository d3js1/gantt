// margin for the svg
const margin = {top: 20, bottom: 30, left: 300, right: 20},
    width = document.getElementById('chart').offsetWidth * 0.9 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// Convertion
const parseTime = d3.timeParse("%m/%d/%Y");
const dateFormat = d3.timeFormat("%m/%d/%Y");


// Create the scales for the diagramme
const X = d3.scaleTime()
    .range([0, width]);
const Y = d3.scaleBand()
    .range([height, 0])
    .padding(0.2);


// Create the svg with margin and its parameters
const svg = d3.select("#chart").append("svg")
    .attr("id", "svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append('g')
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// div to indicate the name and duration of the task
const div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 1);


function PlotGantt() {
    svg.selectAll("*").remove();
    d3.tsv(Getpath(), function (error, data) {

        data.forEach(function (d) {
                d.Startdate = parseTime(d.Startdate);
                d.Enddate = parseTime(d.Enddate);
                d.Duration = +d.Duration;
            }
        );

        Y.domain(data.map(function (d) {
            return d.Title;
        }));

        X.domain(d3.extent(data, function (d) {
            return d.Startdate;
        }));

        SvgActualTime(svg);

        svg.append("g")
            .call(d3.axisLeft(Y).tickSize(10));

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(X).ticks(10));

        svg.selectAll(".bar")
            .data(data)
            .enter()
            .append('path')
            .attr("class", function (d) {
                return d.Pole + " bar";
            })
            .attr("d", function (d) {
                let x11 = X(d.Startdate);
                let x21 = X(d.Enddate);
                let y11 = Y(d.Title);
                let y22 = y11 + Y.bandwidth();
                return 'M' + x11 + ' ' + y11 + ' ' + 'L' + x21 + ' ' + y11 + ' ' + 'L' + x21 + ' ' + y22 + ' ' + 'L' + x11 + ' ' + y22 + ' Z';
            })
            .on("mouseover", function (d) {
                div.transition()
                    .duration(200)
                    .style("opacity", 0.9);

                div.html("<br> <strong> Name: </strong></br>" + d.Title +
                    "<br> <strong>Duration: </strong> </br>" + d.Duration + " jours")
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY) + "px")
            })
            .on("mouseout", function (d) {
                div.transition()
                    .duration(200)
                    .style("opacity", 0)
            });

        let array = [];
        for (let i=0; i<data.length;i++){
             array.push(data[i]['Pole']);

        }
        array = array.sort();
        console.log(array);
        for (let i=0; i<array.length-1;i++){
            let check = i;
            let count = 0;
            while(array[check]==array[i]){
                count ++;
                check ++;
            }
            array.splice(i+1,count-1);
            console.log(count);
        }
        console.log(array);
        CreatSelectPoles(array);


    });

}

// open the file with the path
function ManageView(value) {
    let allasset = $(".bar");
    let asset = $("." + value);
    if (value == "all") {
        for (let i = 0; i < allasset.length; i++) {
            allasset[i].setAttribute("visibility", "visible");
        }
    } else {
        for (let i = 0; i < allasset.length; i++) {
            allasset[i].setAttribute("visibility", "hidden");
        }

        for (let i = 0; i < asset.length; i++) {
            asset[i].setAttribute("visibility", "visible");
        }
    }
}

function addTask() {
    // add a backend to write in the tsv file the new task
}
function SvgActualTime(svg) {
    let verticallinetime = svg.append("line")
        .attr("class", "verticalLine")
        .attr("x1", function (d) {
            return X(new Date());
        })
        .attr("y1", 0)
        .attr("x2", function (d) {
            return X(new Date());
        })
        .attr("y2", height)
        .style("opacity", 1)
        .style("stroke", "black")
        .style("stroke-width", "1");
}

function CreatSelectPoles(data) {
    let pole = d3.select('#pole');
    pole.selectAll('select').remove();
    let selectpolesd3 = pole.append('select')
        .attr('id','selectpoles');
    selectpolesd3.selectAll('option')
        .data(data)
        .enter()
        .append('option')
        .attr('class',function (d) {
          return d + ' poles';
        })
        .attr("value", function (d) {
            return d;
        })
        .text(function (d) {
            return d;
        });
    selectpolesd3.append('option')
        .attr('value', 'all')
        .text('All');

    selectpolesd3.attr('onchange', 'ManageView(value)');

}


function Getpath(){
    let Inputpath = $('#path');
    console.log(Inputpath[0].value);
    let path = Inputpath[0].value;
    return path;
}